/**
 * Retrieves the translation of text.
 *
 * @see https://developer.wordpress.org/block-editor/reference-guides/packages/packages-i18n/
 */
import { __ } from "@wordpress/i18n";

/**
 * React hook that is used to mark the block wrapper element.
 * It provides all the necessary props like the class name.
 *
 * @see https://developer.wordpress.org/block-editor/reference-guides/packages/packages-block-editor/#useblockprops
 */
import { useBlockProps } from "@wordpress/block-editor";
import { useEffect, useState } from "@wordpress/element";

/**
 * Lets webpack process CSS, SASS or SCSS files referenced in JavaScript files.
 * Those files can contain any CSS code that gets applied to the editor.
 *
 * @see https://www.npmjs.com/package/@wordpress/scripts#using-css
 */
import "./editor.scss";

/**
 * The edit function describes the structure of your block in the context of the
 * editor. This represents what the editor will render when the block is used.
 *
 * @see https://developer.wordpress.org/block-editor/reference-guides/block-api/block-edit-save/#edit
 *
 * @return {WPElement} Element to render.
 */
export default function Edit(props) {
	const { attributes, setAttributes } = props;
	const [apiData, setApiData] = useState([]);

	const fetchUser = () => {
		fetch("https://randomuser.me/api/", {
			method: "GET",
		})
			.then((response) => response.json())
			.then((response) => {
				console.log(response);
				let newData = { ...response.results[0] };
				setAttributes({ data: [...apiData, newData] });
				setApiData([...apiData, newData]);
			})
			.catch((error) => {
				console.error(error);
			});
	};

	useEffect(() => {
		fetchUser();
	}, []);

	return (
		<div {...useBlockProps()}>
			{/* {__("Demo Api Block – hello from the editor!", "demo-api-block")} */}
			<button onClick={() => fetchUser()}>Fetch user</button>
			{apiData && (
				<>
					{apiData.map((user) => {
						const {
							dob: { date, age },
							email,
							name: { first, last },
							picture: { thumbnail },
						} = user;
						return (
							<>
								<div>
									<img src={thumbnail}></img>
									<div>Name: {first + " " + last}</div>
									<div>Age: {age}</div>
									<div>DOB: {date}</div>
									<div>Email: {email}</div>
								</div>
							</>
						);
					})}
					<div>heeloooo</div>
				</>
			)}
		</div>
	);
}
